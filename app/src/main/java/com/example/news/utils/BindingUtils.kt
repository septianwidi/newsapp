package com.example.newsapp.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.news.R

@BindingAdapter("image")
fun loadImage( view : ImageView,url : String?){
        Glide.with(view)
            .load(url)
            .centerCrop()
            .placeholder(R.drawable.news_placeholder)
            .error(R.drawable.news_placeholder)
            .fallback(R.drawable.news_placeholder)
            .into(view)
}