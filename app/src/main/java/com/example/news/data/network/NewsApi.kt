package com.example.news.data.network

import com.example.news.data.models.NewsDAO
import com.example.news.data.models.SourceDAO
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("top-headlines")
    suspend fun getTopHeadlinesUS(
        @Query("country") country: String,
        @Query("apiKey") apiKey: String)
            : Response<NewsDAO>

    @GET("sources")
    suspend fun getSources(
        @Query("language") language:String,
        @Query("apiKey") apiKey : String)
            : Response<SourceDAO>

    @GET("top-headlines")
    suspend fun getNewsFromSource(
        @Query("sources") sourceId: String?,
        @Query("apiKey") apiKey: String)
            : Response<NewsDAO>



    companion object{
        operator fun invoke(): NewsApi {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://newsapi.org/v2/")
                .build()
                .create(NewsApi::class.java)
        }
    }

}