package com.example.news.data.models


import com.google.gson.annotations.SerializedName

data class SourceDAO(
    @SerializedName("sources")
    val sources: List<Source>,
    @SerializedName("status")
    val status: String
)