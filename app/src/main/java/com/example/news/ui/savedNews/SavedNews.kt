package com.example.news.ui.savedNews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.news.R
import com.example.news.data.db.NewsDatabase
import com.example.news.ui.BaseFragment
import kotlinx.android.synthetic.main.top_head_lines_us_fragment.*
import kotlinx.coroutines.launch

class SavedNews : BaseFragment(){

    companion object {
        fun newInstance() = SavedNews()
    }

    //private lateinit var viewModel: SavedNewsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.top_head_lines_us_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val pb: ProgressBar =this.loading_bar

        top_news_us_recycler_view.setHasFixedSize(true)
        top_news_us_recycler_view.layoutManager=LinearLayoutManager(requireContext(),LinearLayoutManager.VERTICAL,false)

        //viewModel = ViewModelProviders.of(this).get(SavedNewsViewModel::class.java)

        launch {
            context?.let {
                val news= NewsDatabase(it).getNewsDbDao().getAllSavedNews()
                println("list is : " +news)
                top_news_us_recycler_view.adapter=SavedNewsAdapter(news,pb)
                /*top_news_us_recycler_view.also {
                    LinearLayoutManager(requireContext(),LinearLayoutManager.VERTICAL,false)
                    it.setHasFixedSize(true)
                    it.adapter=SavedNewsAdapter(news,pb)
                }*/
            }
        }

    }

}
