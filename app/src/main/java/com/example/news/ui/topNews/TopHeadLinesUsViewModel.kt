package com.example.news.ui.topNews

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.news.utils.Coroutines
import com.example.news.data.models.NewsDAO
import com.example.news.data.repositories.NewsRepository
import kotlinx.coroutines.Job

class TopHeadLinesUsViewModel(private val repository: NewsRepository) : ViewModel() {

    private lateinit var job: Job
    private val _topNewsUs=MutableLiveData<NewsDAO>()
    val topNewsUs : LiveData<NewsDAO>
        get() = _topNewsUs

    fun getTopNewsUs(){
        job = Coroutines.ioThenMain(
            { repository.getNewsList() },
            { _topNewsUs.value = it })
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }



}
